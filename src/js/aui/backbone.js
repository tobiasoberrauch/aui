// BEWARE: The following is an unused import with side-effects
import _ from './underscore'; // eslint-disable-line no-unused-vars
import Backbone from 'backbone';

if (!window.Backbone) {
    window.Backbone = Backbone;
}

export default window.Backbone;
