import camelCase from 'camelcase';
import element from '../element';
import skate from 'skatejs';

export default function (name, definition) {
    return element[camelCase(name)] = skate(`aui-${name}`, definition);
}
