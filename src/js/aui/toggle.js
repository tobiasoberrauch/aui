'use strict';

import './spin';
import { INPUT_SUFFIX } from './internal/constants';
import $ from 'jquery';
import enforce from './internal/enforcer';
import skate from 'skatejs';
import WeakMap from 'weakmap';

const internalElementMap = new WeakMap();

function getTooltipContent (elem) {
    return internalElementMap.get(elem).input.checked ? elem.tooltipOn : elem.tooltipOff;
}

function setDisabledForLabels (elem, disabled) {
    if (!elem.id) {
        return;
    }
    Array.prototype.forEach.call(document.querySelectorAll(`aui-label[for="${elem.id}"]`), function (label) {
        label.disabled = disabled;
    });
}

/**
 * Workaround to prevent pressing SPACE on busy state.
 * Preventing click event still makes the toggle flip and revert back.
 * So on CSS side, the input has "pointer-events: none" on busy state.
 */
function bindEventsToInput (elem) {
    const input = internalElementMap.get(elem).input;
    input.addEventListener('keydown', function (e) {
        if (elem.busy && e.keyCode ===  AJS.keyCode.SPACE) {
            e.preventDefault();
        }
    });
    // prevent toggle can be trigger through SPACE key on Firefox
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        input.addEventListener('click', function (e) {
            if (elem.busy) {
                e.preventDefault();
            }
        });
    }
}

const render = skate.render.html(function () {
    return `
        <input type="checkbox" class="aui-toggle-input">
            <span class="aui-toggle-view">
            <span class="aui-toggle-tick aui-icon aui-icon-small aui-iconfont-success"></span>
            <span class="aui-toggle-cross aui-icon aui-icon-small aui-iconfont-close-dialog"></span>
        </span>
    `;
});

function linkPropertyToInputAttribute(elem, attributeName, newValue) {
    const input = internalElementMap.get(elem).input;
    if (newValue === undefined) {
        input.removeAttribute(attributeName);
    } else {
        input.setAttribute(attributeName, newValue);
    }
}

export default skate('aui-toggle', {
    created: function (elem) {
        render(elem);
        internalElementMap.set(elem, {
            cross: elem.querySelector('.aui-toggle-cross'),
            input: elem.querySelector('input'),
            tick: elem.querySelector('.aui-toggle-tick')
        });
    },
    ready: function (elem) {
        $(elem).tooltip({title: getTooltipContent.bind(null, elem), gravity: 's', hoverable: false});
        bindEventsToInput(elem);
    },
    attached: function (elem) {
        enforce(elem).attributeExists('label');
    },
    events: {
        click: function clickHandler(e) {
            const input = internalElementMap.get(this).input;
            if (!this.disabled && !this.busy && e.target !== input) {
                input.click();
            }
            this.checked = input.checked;
        }
    },
    properties: {
        busy: skate.properties.boolean({
            attribute: true,
            get (elem) {
                return internalElementMap.get(elem).input.getAttribute('aria-busy') === 'true';
            },
            set (elem, data) {
                const cross = internalElementMap.get(elem).cross;
                const input = internalElementMap.get(elem).input;
                const newValue = data.newValue;
                const tick = internalElementMap.get(elem).tick;

                if (newValue) {
                    input.setAttribute('aria-busy', 'true');
                    input.indeterminate = true;
                    if (elem.checked) {
                        // .indeterminate-checked is to workaround Chrome and Safari no longer
                        // matching :checked when .indeterminate is set to true.
                        $(input).addClass('indeterminate-checked');
                        $(tick).spin({zIndex: null});
                    } else {
                        $(cross).spin({zIndex: null, color: 'black'});
                    }
                } else {
                    $(input).removeClass('indeterminate-checked');
                    input.indeterminate = false;
                    input.removeAttribute('aria-busy');
                    $(cross).spinStop();
                    $(tick).spinStop();
                }

                setDisabledForLabels(elem, newValue);
            }
        }),
        checked: skate.properties.boolean({
            attribute: true,
            set (elem, data) {
                internalElementMap.get(elem).input.checked = data.newValue;
            }
        }),
        disabled: skate.properties.boolean({
            attribute: true,
            set (elem, data) {
                internalElementMap.get(elem).input.disabled = data.newValue;
            }
        }),
        form: skate.properties.string({
            attribute: true,
            get (elem) {
                return document.getElementById(internalElementMap.get(elem)._formId) || null;
            },
            set (elem, data) {
                linkPropertyToInputAttribute(elem, 'form', data.newValue);
                // we need to save the form element separately, because according to the HTML5
                // spec, dynamically setting the "form" attribute is not defined and IE11 for example
                // just ignores it
                internalElementMap.get(elem)._formId = data.newValue;
            }
        }),
        id: skate.properties.string({
            attribute: true,
            set (elem, data) {
                linkPropertyToInputAttribute(elem, 'id', data.newValue ? data.newValue + INPUT_SUFFIX : data.newValue);
            }
        }),
        label: skate.properties.string({
            attribute: true,
            set (elem, data) {
                linkPropertyToInputAttribute(elem, 'aria-label', data.newValue);
            }
        }),
        name: skate.properties.string({
            attribute: true,
            set (elem, data) {
                linkPropertyToInputAttribute(elem, 'name', data.newValue);
            }
        }),
        tooltipOn: skate.properties.string({
            attribute: true,
            default: AJS.I18n.getText('aui.toggle.on'),
            set (elem, data) {
                linkPropertyToInputAttribute(elem, 'tooltip-on', data.newValue);
            }
        }),
        tooltipOff: skate.properties.string({
            attribute: true,
            default: AJS.I18n.getText('aui.toggle.off'),
            set (elem, data) {
                linkPropertyToInputAttribute(elem, 'tooltip-off', data.newValue);
            }
        }),
        value: skate.properties.string({
            attribute: true,
            default: 'on',
            set (elem, data) {
                const input = internalElementMap.get(elem).input;
                input.value = data.newValue;
            }
        })
    },
    prototype: {
        focus: function () {
            internalElementMap.get(this).input.focus();
            return this;
        }
    }
});
