'use strict';

import globalize from './internal/globalize';
import { fn as deprecateFn } from './internal/deprecation';
import onTextResize from './on-text-resize';

function unbindTextResize (f) {
    for (var i = 0, ii = onTextResize['on-text-resize'].length; i < ii; i++) {
        if (onTextResize['on-text-resize'][i] === f) {
            return onTextResize['on-text-resize'].splice(i, 1);
        }
    }
}

unbindTextResize = deprecateFn(unbindTextResize, 'unbindTextResize', {
    removeInVersion: '6.0.0',
    sinceVersion: '5.9.0'
});

globalize('unbindTextResize', unbindTextResize);

export default unbindTextResize;
