'use strict';

import './button';
import './i18n';
import './spin';
import $ from './jquery';
import amdify from './internal/amdify';
import component from './internal/component';
import keyCode from './key-code';
import ProgressiveDataSet from './progressive-data-set';
import skate from 'skatejs';
import state from './internal/state';
import SuggestionModel from './internal/select/suggestion-model';
import SuggestionsModel from './internal/select/suggestions-model';
import SuggestionsView from './internal/select/suggestions-view';
import skateTemplateHtml from 'skatejs-template-html';
import uniqueId from './unique-id';

const DESELECTED = -1;
const NO_HIGHLIGHT = -1;
const DEFAULT_SS_PDS_SIZE = 20;

function clearElementImage (element) {
    element._input.removeAttribute('style');
    $(element._input).removeClass('aui-select-has-inline-image');
}

function deselect (element) {
    element._select.selectedIndex = DESELECTED;
    clearElementImage(element);
}

function hasResults (element) {
    return element._suggestionModel.getNumberOfResults();
}

function waitForAssistive (callback) {
    setTimeout(callback, 50);
}

function setBusyState (element) {
    if (!element._button.isBusy()) {
        element._button.busy();
        element._input.setAttribute('aria-busy', 'true');
        element._dropdown.setAttribute('aria-busy', 'true');
    }
}

function setIdleState (element) {
    element._button.idle();
    element._input.setAttribute('aria-busy', 'false');
    element._dropdown.setAttribute('aria-busy', 'false');
}

function matchPrefix (model, query) {
    const value = model.get('label').toLowerCase();
    return value.indexOf(query.toLowerCase()) === 0;
}

function hideDropdown (element) {
    element._suggestionsView.hide();
    element._input.setAttribute('aria-expanded', 'false');
}

function setInitialVisualState (element) {
    const initialHighlightedItem = hasResults(element) ? 0 : NO_HIGHLIGHT;

    element._suggestionModel.highlight(initialHighlightedItem);

    hideDropdown(element);
}

function setElementImage (element, imageSource) {
    $(element._input).addClass('aui-select-has-inline-image');
    element._input.setAttribute('style', 'background-image: url(' + imageSource + ')');
}

function suggest (element, autoHighlight, query) {
    element._autoHighlight = autoHighlight;

    if (query === undefined) {
        query = element._input.value;
    }

    element._progressiveDataSet.query(query);
}

function setInputImageToHighlightedSuggestion (element) {
    const imageSource = (element._suggestionModel.highlighted() && element._suggestionModel.highlighted().get('img-src'));
    if (imageSource) {
        setElementImage(element, imageSource);
    }
}

function setValueAndDisplayFromModel (element, model) {
    if (!model) {
        return;
    }

    const option = document.createElement('option');
    const select = element._select;
    const value = model.get('value') || model.get('label');

    option.setAttribute('selected', '');
    option.setAttribute('value', value);
    option.textContent = model.getLabel();

    // Sync element value.
    element._input.value = option.textContent;

    select.innerHTML = '';
    select.options.add(option);
    skate.emit(select, 'change');
}

function clearValue (element) {
    element._input.value = '';
    element._select.innerHTML = '';
}

function selectHighlightedSuggestion (element) {
    setValueAndDisplayFromModel(element, element._suggestionModel.highlighted());
    setInputImageToHighlightedSuggestion(element);
    setInitialVisualState(element);
}

function serializeOption (option) {
    const json = {};
    if (option.hasAttribute('img-src')) {
        json['img-src'] = option.getAttribute('img-src');
    }
    json.value = option.getAttribute('value') || option.textContent;
    json.label = option.textContent;

    return json;
}

function convertOptionToModel (option) {
    return new SuggestionModel(serializeOption(option));
}

function convertOptionsToModels (element) {
    const models = [];

    for (let i = 0; i < element._datalist.children.length; i++) {
        const option = element._datalist.children[i];
        models.push(convertOptionToModel(option));
    }

    return models;
}

function clearAndSet (element, data) {
    element._suggestionModel.set();
    element._suggestionModel.set(data.results);
}

function getActiveId (select) {
    const active = select._dropdown.querySelector('.aui-select-active');
    return active && active.id;
}

function getIndexInResults (id, results) {
    const resultsIds = $.map(results, function (result) {
        return result.id;
    });

    return resultsIds.indexOf(id);
}

function createNewValueModel (element) {
    const option = new Option();
    option.setAttribute('value', element._input.value);
    const newValueSuggestionModel = convertOptionToModel(option);
    newValueSuggestionModel.set('new-value', true);
    return newValueSuggestionModel;
}

function initialiseProgressiveDataSet (element) {
    element._progressiveDataSet = new ProgressiveDataSet(convertOptionsToModels(element), {
        model: SuggestionModel,
        matcher: matchPrefix,
        queryEndpoint: element._queryEndpoint,
        maxResults: DEFAULT_SS_PDS_SIZE
    });

    element._isSync = element._queryEndpoint ? false : true;

    // Progressive data set should indicate whether or not it is busy when processing any async requests.
    // Check if there's any active queries left, if so: set spinner and state to busy, else set to idle and remove
    // the spinner.
    element._progressiveDataSet.on('activity', function () {
        if (element._progressiveDataSet.activeQueryCount && !element._isSync) {
            setBusyState(element);
            state(element).set('should-flag-new-suggestions', false);
        } else {
            setIdleState(element);
            state(element).set('should-flag-new-suggestions', true);
        }
    });

    // Progressive data set doesn't do anything if the query is empty so we
    // must manually convert all data list options into models.
    //
    // Otherwise progressive data set can do everything else for us:
    // 1. Sync matching
    // 2. Async fetching and matching
    element._progressiveDataSet.on('respond', function (data) {
        let optionToHighlight;

        // This means that a query was made before the input was cleared and
        // we should cancel the response.
        if (data.query && !element._input.value) {
            return;
        }

        if (state(element).get('should-cancel-response')) {
            if (!element._progressiveDataSet.activeQueryCount) {
                state(element).set('should-cancel-response', false);
            }

            return;
        }

        if (!data.query) {
            data.results = convertOptionsToModels(element);
        }

        const isInputExactMatch = getIndexInResults(element._input.value, data.results) !== -1;
        const isInputEmpty = !element._input.value;

        if (element.hasAttribute('can-create-values') && !isInputExactMatch && !isInputEmpty) {
            data.results.push(createNewValueModel(element));
        }

        if (!state(element).get('should-include-selected')) {
            const indexOfValueInResults = getIndexInResults(element.value, data.results);

            if (indexOfValueInResults >= 0) {
                data.results.splice(indexOfValueInResults, 1);
            }
        }

        clearAndSet(element, data);
        optionToHighlight = element._suggestionModel.highlighted() || data.results[0];

        if (element._autoHighlight) {
            element._suggestionModel.setHighlighted(optionToHighlight);
            waitForAssistive(function () {
                element._input.setAttribute('aria-activedescendant', getActiveId(element));
            });
        }

        element._input.setAttribute('aria-expanded', 'true');

        const isAsync = !element._isSync;
        const hasActiveSuggestion = !!element._suggestionsView.getActive();
        const shouldFlagNewSuggestions = state(element).get('should-flag-new-suggestions');
        const shouldUpdateStatus = isAsync && hasActiveSuggestion && shouldFlagNewSuggestions;

        if (shouldUpdateStatus) {
            element.querySelector('.aui-select-status').innerHTML = AJS.I18n.getText('aui.select.new.suggestions');
        }

        element._suggestionsView.show();

        if (element._autoHighlight) {
            waitForAssistive(function () {
                element._input.setAttribute('aria-activedescendant', getActiveId(element));
            });
        }
    });
}

function associateDropdownAndTrigger (element) {
    element._dropdown.id = element._listId;
    element.querySelector('button').setAttribute('aria-controls', element._listId);
}

function bindHighlightMouseover (element) {
    $(element._dropdown).on('mouseover', 'li', function (e) {
        if (hasResults(element)) {
            element._suggestionModel.highlight($(e.target).index());
        }
    });
}

function bindSelectMousedown (element) {
    $(element._dropdown).on('mousedown', 'li', function (e) {
        if (hasResults(element)) {
            element._suggestionModel.highlight($(e.target).index());
            selectHighlightedSuggestion(element);
            element._suggestionsView.hide();
            element._input.removeAttribute('aria-activedescendant');
        } else {
            return false;
        }
    });
}

function initialiseValue (element) {
    const option = element._datalist.querySelector('aui-option[selected]');

    if (option) {
        setValueAndDisplayFromModel(element, convertOptionToModel(option));
    }
}

function isQueryInProgress (element) {
    return element._progressiveDataSet.activeQueryCount > 0;
}

function focusInHandler (element) {
    //if there is a selected value the single select should do an empty
    //search and return everything
    const searchValue = element.value ? '' : element._input.value;
    const isInputEmpty = element._input.value === '';
    state(element).set('should-include-selected', isInputEmpty);
    suggest(element, true, searchValue);
}

function cancelInProgressQueries (element) {
    if (isQueryInProgress(element)) {
        state(element).set('should-cancel-response', true);
    }
}

function getSelectedLabel(element) {
    if (element._select.selectedIndex >= 0) {
        return element._select.options[element._select.selectedIndex].textContent;
    }
}

function handleInvalidInputOnFocusOut (element) {
    const selectCanBeEmpty = !element.hasAttribute('no-empty-values');
    const selectionIsEmpty = !element._input.value;
    const selectionNotExact = element._input.value !== getSelectedLabel(element);
    const selectionNotValid = selectionIsEmpty || selectionNotExact;

    if (selectionNotValid) {
        if (selectCanBeEmpty) {
            deselect(element);
        } else {
            element._input.value = getSelectedLabel(element);
        }
    }
}

function handleHighlightOnFocusOut(element) {
    // Forget the highlighted suggestion.
    element._suggestionModel.highlight(NO_HIGHLIGHT);
}

function focusOutHandler (element) {
    cancelInProgressQueries(element);
    handleInvalidInputOnFocusOut(element);
    handleHighlightOnFocusOut(element);
    hideDropdown(element);
}

function handleTabOut (element) {
    const isSuggestionViewVisible = element._suggestionsView.isVisible();
    if (isSuggestionViewVisible) {
        selectHighlightedSuggestion(element);
    }
}

var SingleSelect = component('select', {
    render: function (element) {
        // Custom renderer, passing the template string from `render` into our templater.
        skateTemplateHtml(`
            <input type="text" class="text" autocomplete="off" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false">
            <select></select>
            <datalist>
                <content select="aui-option"></content>
            </datalist>
            <button class="aui-button" role="button" tabindex="-1" type="button"></button>
            <div class="aui-popover" role="listbox" data-aui-alignment="bottom left">
                <ul class="aui-optionlist" role="presentation"></ul>
            </div>
            <div class="aui-select-status assistive" aria-live="polite" role="status"></div>
        `)(element);

        element._listId = uniqueId();
        element._input = element.querySelector('input');
        element._select = element.querySelector('select');
        element._dropdown = element.querySelector('.aui-popover');
        element._datalist = element.querySelector('datalist');
        element._button = element.querySelector('button');
    },
    ready: function (element) {
        element._suggestionsView = new SuggestionsView(element._dropdown, element._input);
        element._suggestionModel = new SuggestionsModel();

        element._suggestionModel.onChange = function (oldSuggestions) {
            const suggestionsToAdd = [];

            element._suggestionModel._suggestions.forEach(function (newSuggestion) {
                const inArray = oldSuggestions.some(function (oldSuggestion) {
                    return newSuggestion.id === oldSuggestion.id;
                });

                if (!inArray) {
                    suggestionsToAdd.push(newSuggestion);
                }
            });

            element._suggestionsView.render(suggestionsToAdd, oldSuggestions.length, element._listId);
        };

        element._suggestionModel.onHighlightChange = function () {
            const active = element._suggestionModel.highlightedIndex();
            element._suggestionsView.setActive(active);
            element._input.setAttribute('aria-activedescendant', getActiveId(element));
        };

        initialiseProgressiveDataSet(element);
        associateDropdownAndTrigger(element);
        element._input.setAttribute('aria-controls', element._listId);
        element.setAttribute('tabindex', '-1');
        bindHighlightMouseover(element);
        bindSelectMousedown(element);
        initialiseValue(element);
        setInitialVisualState(element);
        setInputImageToHighlightedSuggestion(element);
    },
    events: {
        'blur input': function () {
            focusOutHandler(this);
        },
        'mousedown button': function () {
            var isActiveElement = document.activeElement === this._input;
            var isAriaVisible = this._dropdown.getAttribute('aria-hidden') === 'false';
            if (isActiveElement && isAriaVisible) {
                state(this).set('prevent-open-on-button-click', true);
            }
        },
        'click input': function () {
            focusInHandler(this);
        },
        'click button': function () {
            const data = state(this);

            if (data.get('prevent-open-on-button-click')) {
                data.set('prevent-open-on-button-click', false);
            } else {
                this.focus();
            }
        },
        input: function () {
            if (!this._input.value) {
                hideDropdown(this);
            } else {
                state(this).set('should-include-selected', true);
                suggest(this, true);
            }
        },
        'keydown input': function (e) {
            const currentValue = this._input.value;
            let handled = false;

            if (e.keyCode === keyCode.ESCAPE) {
                cancelInProgressQueries(this);
                hideDropdown(this);
                return;
            }

            const isSuggestionViewVisible = this._suggestionsView.isVisible();

            if (isSuggestionViewVisible && hasResults(this)) {
                if (e.keyCode === keyCode.ENTER) {
                    cancelInProgressQueries(this);
                    selectHighlightedSuggestion(this);
                    e.preventDefault();
                } else if (e.keyCode === keyCode.TAB) {
                    handleTabOut(this);
                    handled = true;
                } else if (e.keyCode === keyCode.UP) {
                    this._suggestionModel.highlightPrevious();
                    e.preventDefault();
                } else if (e.keyCode === keyCode.DOWN) {
                    this._suggestionModel.highlightNext();
                    e.preventDefault();
                }
            } else if (e.keyCode === keyCode.UP || e.keyCode === keyCode.DOWN) {
                focusInHandler(this);
                e.preventDefault();
            }

            handled = handled || e.defaultPrevented;
            setTimeout(function emulateCrossBrowserInputEvent () {
                if (this._input.value !== currentValue && !handled) {
                    skate.emit(this, 'input');
                }
            }.bind(this), 0);
        }
    },
    properties: {
        id: {
            attribute: true,
            set (element, changeData) {
                const input = element.querySelector('input');
                if (changeData.newValue) {
                    input.setAttribute('id', changeData.newValue + '-input');
                } else {
                    input.removeAttribute('id');
                }
            }
        },
        name: {
            attribute: true,
            set (element, changeData) {
                if (changeData.newValue === undefined) {
                    element.querySelector('select').removeAttribute('name');
                } else {
                    element.querySelector('select').setAttribute('name', changeData.newValue);
                }
            }
        },
        placeholder: {
            attribute: true,
            set (element, changeData) {
                if (changeData.newValue === undefined) {
                    element.querySelector('input').removeAttribute('placeholder');
                } else {
                    element.querySelector('input').setAttribute('placeholder', changeData.newValue);
                }
            }
        },
        src: {
            attribute: true,
            set (element, changeData) {
                element._queryEndpoint = changeData.newValue;
            }
        },
        value: {
            attribute: true,
            get (element) {
                const selected = element._select.options[element._select.selectedIndex];
                return selected ? selected.value : '';
            },
            set (element, changeData) {
                if (changeData.newValue === '') {
                    clearValue(element);
                } else if (changeData.newValue) {
                    const data = element._progressiveDataSet;
                    let model = data.findWhere({
                        value: changeData.newValue
                    }) || data.findWhere({
                        label: changeData.newValue
                    });

                    // Create a new value if allowed and the value doesn't exist.
                    if (!model && element.hasAttribute('can-create-values')) {
                        model = new SuggestionModel({value: changeData.newValue, label: changeData.newValue});
                    }

                    setValueAndDisplayFromModel(element, model);
                }
            }
        }
    },
    prototype: {
        get displayValue () {
            return this._input.value;
        },
        blur: function () {
            this._input.blur();
            focusOutHandler(this);
            return this;
        },
        focus: function () {
            this._input.focus();
            focusInHandler(this);
            return this;
        }
    }
});

amdify('aui/select', SingleSelect);
export default SingleSelect;
