'use strict';

import './i18n';
import './trigger';
import component from './internal/component';
import generateUniqueId from './unique-id';
import skate from 'skatejs';


const RevealText = component('reveal-text', {
    render: function(element) {
        const open = element.open;
        element.setAttribute('aria-expanded', open);
        if (!element.id) {
            element.id = generateUniqueId();
        }

        element.innerHTML = `
             <div contents>${element.innerHTML}</div>
             <button data-aui-trigger aria-controls="${element.id}" aria-pressed="${open}">
                 <span open-text>${element.openText}</span>
                 <span closed-text>${element.closedText}</span>
             </button>`;
    },
    properties: {
        open: skate.properties.boolean({
            attribute: true,
            default: false,
            set (element, data) {
                skate.emit(element, data.newValue ? 'aui-show' : 'aui-hide');
            }
        }),
        closedText: skate.properties.string({
            attribute: true,
            default: AJS.I18n.getText('aui.revealtext.closed'),
            set: function (element, data) {
                element.querySelector(`[aria-controls="${element.id}"] [closed-text]`).textContent = data.newValue;
            }
        }),
        openText: skate.properties.string({
            attribute: true,
            default: AJS.I18n.getText('aui.revealtext.open'),
            set (element, data) {
                element.querySelector(`[aria-controls="${element.id}"] [open-text]`).textContent = data.newValue;
            }
        })
    },
    prototype: {
        message: function(message) {
            // Handle click messages from the trigger.
            if (message.type === 'click') {
                this.open = !this.open;
            }
        }
    }
});

export default RevealText;
