'use strict';

import globalize from './internal/globalize';
import skate from 'skatejs';

var lozenge = skate('aui-lozenge', {
    properties: {
        display: {
            attribute: true
        },
        type: {
            attribute: true
        }
    }

});

export default globalize('lozenge', lozenge);
