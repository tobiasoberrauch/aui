---
component: Reveal text
layout: main-layout.html
analytics:
  pageCategory: component
  component: reveal-text
---

<a href="https://design.atlassian.com/latest/product/patterns/reveal-text/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<div class="aui-message aui-message-info">
    This documentation is for the Reveal text <strong>web component</strong> API, which replaces the reveal text pattern for the expander markup API.
    <ul class="aui-nav-actions-list"><li><a href="expander.html">View documentation for the expander markup API.</a></li></ul>
</div>

<p>
    A container used when you need to show users one or more small snippets of a larger body of text in order
    to keep the user interface lightweight and scannable, as described in the
    <a href="https://design.atlassian.com/latest/product/patterns/reveal-text/">Atlassian design guidelines</a>.
</p>


<h3>Status</h3>
<table class="aui summary">
    <tbody>
    <tr>
        <th>API status:</th>
        <td><aui-lozenge type="success">general</aui-lozenge>
    </tr>
    <tr>
        <th>Included in AUI core?</th>
        <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web resource key.</td>
    </tr>
    <tr>
        <th>Web resource key:</th>
        <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-reveal-text"><code>com.atlassian.auiplugin:aui-reveal-text</code>
        </td>
    </tr>
    <tr>
        <th>Experimental since:</th>
        <td>5.10</td>
    </tr>
    </tbody>
</table>

<aui-docs-contents></aui-docs-contents>

<h3 id="example">Example</h3>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <aui-reveal-text open closed-text="Show more text" open-text="Show less text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum.
        </aui-reveal-text>
    </noscript>
</aui-docs-example>


<h3 id="behaviour">Behaviour</h3>

<h4 id="opening">Opening</h4>

<p>
    A reveal text will, by default, open when a user clicks its trigger, but can be made to open
</p>
<ul>
    <li><a href="#programmatically-opening">programmatically</a>, or</li>
    <li><a href="#opening-at-page-load">automatically at page load</a>.
</ul>

<h5 id="programmatically-opening">Programmatically opening</h5>
<p>
    To programmatically open a reveal text component, set its <code class="first-use">open</code> property or add the
    <code>open</code> boolean attribute:
</p>

<noscript is="aui-docs-code" type="text/js">
    var revealText = document.getElementById('my-reveal-text');

    // Opening
    revealText.open = true;
    revealText.setAttribute('open', '');  // Equivalent to line above.

    // Closing
    revealText.open = false;
    revealText.removeAttribute('open');  // Equivalent to line above.
</noscript>

<h5 id="opening-at-page-load">Opening at page load</h5>
<p>
    To make a reveal text component open at page load, simply specify the <code>open</code> boolean attribute:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <aui-reveal-text open>
            <p>Reveal text component that starts open.</p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum.
        </aui-reveal-text>
    </noscript>
</aui-docs-example>


<h3 id="appearance">Appearance</h3>

<h4 id="trigger-text">Trigger text</h4>

<p>
    The text shown in the trigger when the reveal text component is open or closed can be configured using the
    <code class="first-use">open-text</code> and <code class="first-use">closed-text</code> attributes respectively:
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <aui-reveal-text closed-text="Show full text" open-text="Hide full text">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum.
        </aui-reveal-text>
    </noscript>
</aui-docs-example>

<p>Or by setting the <code class="first-use">openText</code> or <code class="first-use">closedText</code> properties:</p>

<noscript is="aui-docs-code" type="text/js">
    var revealText = document.getElementById('my-reveal-text');
    revealText.openText = 'Show less text';
    revealText.closedText = 'Show more text';
</noscript>

<div class="aui-message aui-message-hint">
    <h5>Default trigger text</h5>
    <p>
        If no trigger text is specified, the reveal text will display <code>"Show more"</code> when it is closed and <code>"Show less"</code> when it is open by default.
    </p>
</div>


<h3 id="api-reference">API Reference</h3>

<h4 id="attributes-and-properties">Attributes and properties</h4>

<table class="aui docs-table">
    <thead>
    <tr>
        <th>Name</th>
        <th>Attribute</th>
        <th>Property</th>
        <th>Type</th>
        <th class="description">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code id="api-reference-id">id</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>The <code>id</code> of the element. If not provided, AUI will generate a unique <code>id</code> for the <code>aui-reveal-text</code> element.</p>
        </td>
    </tr>
    <tr>
        <td><code id="api-reference-open">open</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>
            <p>When set it either hides or shows the contents of the component based on whether the incoming value is falsy or truthy. When accessed it will return whether or not the reveal text component is open.</p>
        </td>
    </tr>
    <tr>
        <td><code id="api-reference-open-text">open-text</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>The text shown in the trigger when the reveal text component is open.</p>
            <p>Defaults to <code>"Show less"</code>.</p>
        </td>
    </tr>
    <tr>
        <td><code id="api-reference-closed-text">closed-text</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>The text shown in the trigger when the reveal text component is closed.</p>
            <p>Defaults to <code>"Show more"</code>.</p>
        </td>
    </tr>
    </tbody>
</table>


<h4 id="methods">Methods</h4>
<p>There are no methods.</p>


<h4 id="events">Events</h4>

<p>Events are triggered when a reveal text component is shown or hidden. These events are triggered natively on the component.</p>
<p>You can bind to the element for instance-specific events, or rely on event bubbling and bind to the document to receive events for every show and hide.</p>

<table class="aui">
    <thead>
    <tr>
        <th>Event</th>
        <th class="description">Description</th>
        <th>Preventable</th>
        <th>Bubbles</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td id="api-reference-aui-show">aui-show</td>
        <td>Triggered before the full text is shown.</td>
        <td>No.</td>
        <td>Yes</td>
    </tr>
    <tr>
        <td id="api-reference-aui-hide">aui-hide</td>
        <td>Triggered before the full text is hidden.</td>
        <td>No.</td>
        <td>Yes</td>
    </tr>
    </tbody>
</table>
