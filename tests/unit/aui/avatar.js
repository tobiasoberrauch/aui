'use strict';

import '../../../src/js/aui/avatar';
import helpers from '../../helpers/all';
import skate from 'skatejs';
import * as logger from '../../../src/js/aui/internal/log';

describe('aui/avatar', function () {
    function createAvatar (opts) {
        opts = opts || {};
        const src = opts.src ? `src="${opts.src}"` : '';
        const alt = opts.alt ? `alt="${opts.alt}"` : '';
        const type = opts.type ? `type="${opts.type}"` : '';
        const size = opts.size ? `size="${opts.size}"` : '';

        const avatar = helpers.fixtures({
            avatar: `<aui-avatar ${src} ${alt} ${type} ${size}></aui-avatar>`
        }).avatar;

        skate.init(avatar);
        return avatar;
    }

    function getImg (avatar) {
        return avatar.querySelector('img');
    }

    function expectAttributeAndPropToEqual(el, name, val) {
        expect(el.getAttribute(name)).to.equal(val);
        expect(el[name]).to.equal(val);
    }

    describe('construction and initialization', function () {
        beforeEach(function () {
            sinon.spy(logger, 'error');
        });

        afterEach(function () {
            logger.error.restore();
        });

        it('logs an error if the src attribute is missing', function () {
            createAvatar({alt: 'alt text'});
            expect(logger.error.calledOnce).to.equal(true);
        });

        it('logs an error if the alt attribute is missing', function () {
            createAvatar({src: 'base/src/less/images/icons/core/icon-move-d.png'});
            expect(logger.error.calledOnce).to.equal(true);
        });

        it('does not log any errors if the src and alt attributes are provided', function () {
            createAvatar({src: 'base/src/less/images/icons/core/icon-move-d.png', alt: 'alt text'});
            expect(logger.error.calledOnce).to.equal(false);
        });

        it('does not log any errors if the src and alt attributes are empty strings', function () {
            createAvatar({src: '', alt: ''});
            expect(logger.error.calledOnce).to.equal(false);
        });

        it('copies the src attribute to the internal img element', function () {
            const srcValue = 'base/src/less/images/icons/core/icon-move-d.png';
            const avatar = createAvatar({src: srcValue});
            expect(getImg(avatar).getAttribute('src')).to.equal(srcValue);
        });

        it('copies the alt attribute to the internal img element', function () {
            const altValue = '/alt/value';
            const avatar = createAvatar({alt: altValue});
            expect(getImg(avatar).getAttribute('alt')).to.equal(altValue);
        });
    });

    describe('size attribute/property', function () {
        const VALID_SIZES = ['xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge', 'xxxlarge'];

        it('can be initialised to "small"', function () {
            const avatar = createAvatar({size: 'small'});
            expectAttributeAndPropToEqual(avatar, 'size', 'small');
        });

        it('defaults to "medium" if not set', function () {
            const avatar = createAvatar();
            expectAttributeAndPropToEqual(avatar, 'size', 'medium');
        });

        it('defaults to "medium" if an invalid value is set', function (done) {
            const avatar = createAvatar({size: 'large'});
            avatar.size = 'invalid size';

            helpers.afterMutations(function () {
                expectAttributeAndPropToEqual(avatar, 'size', 'medium');
                done();
            });
        });

        it('returns "medium" when getting the property and it is not set', function () {
            const avatar = createAvatar();
            expect(avatar.size).to.equal('medium');
        });

        VALID_SIZES.forEach(function (size) {
            it(`takes "${size}" as a valid value`, function (done) {
                const avatar = createAvatar();
                avatar.size = size;

                helpers.afterMutations(function () {
                    expectAttributeAndPropToEqual(avatar, 'size', size);
                    done();
                });
            });
        });
    });

    describe('type attribute/property', function () {
        it('can be initialised to "project"', function () {
            const avatar = createAvatar({type: 'project'});
            expectAttributeAndPropToEqual(avatar, 'type', 'project');
        });

        it('defaults to "user" if not set', function () {
            const avatar = createAvatar();
            expectAttributeAndPropToEqual(avatar, 'type', 'user');
        });

        it('returns "user" when getting the property and it is not set', function () {
            const avatar = createAvatar();
            expect(avatar.type).to.equal('user');
        });
    });
});
