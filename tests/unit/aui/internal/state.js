import state from '../../../../src/js/aui/internal/state';

describe('aui/internal/state', function () {
    function createElement () {
        return document.createElement('div');
    }

    it('get()', function () {
        const element = createElement();
        expect(state(element).get('testState')).to.equal(undefined);
    });

    it('set()', function () {
        const element = createElement();
        state(element).set('testState', 'testValue');
        expect(state(element).get('testState')).to.equal('testValue');
    });

    it('remove()', function () {
        const element = createElement();
        state(element).set('testState', 'testValue');
        state(element).remove('testState');
        expect(state(element).get('testState')).to.equal(undefined);
    });
});
