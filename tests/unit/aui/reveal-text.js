'use strict';

import '../../../src/js/aui/reveal-text';
import helpers from '../../helpers/all';

describe('aui/reveal-text', function() {
    describe('Reveal Text Tests -', function () {
        let revealText;

        function initialiseRevealText(options) {
            if (!options) {
                options = {};
            }
            const attributes = (options.open ? ' open' : '') +
                (options.openText ? ` open-text="${options.openText}"` : '') +
                (options.closedText ? ` closed-text="${options.closedText}"` : '');

            revealText = helpers.fixtures({
                revealText: `<aui-reveal-text ${attributes}>${options.content ? options.content: 'Sample content here'}</aui-reveal-text>`
            }, true).revealText;
        }

        function getTrigger(revealText) {
            return revealText.querySelector('[data-aui-trigger]');
        }

        function getTriggerText(revealText) {
            return revealText.querySelector(`[data-aui-trigger] ${revealText.open ? '[open-text]' : '[closed-text]'}`).textContent;
        }

        function clickTrigger(revealText) {
            helpers.click(getTrigger(revealText));
        }


        describe('reveal-text basic behaviour -', function () {
            beforeEach(function () {
                initialiseRevealText();
            });

            it('can be initialised', function () {
                expect(revealText).to.exist;
            });

            it('contains a trigger', function () {
                expect(getTrigger(revealText)).to.exist;
            });

            it('is initialised to be closed by default', function () {
                expect(revealText.open).to.equal(false, 'starts closed');
            });

            it('has triggers associated with reveal text element', function () {
                expect(getTrigger(revealText).getAttribute('aria-controls')).to.equal(revealText.id, 'trigger element associated with aui-reveal-text element');
            });

            it('opens when trigger is clicked', function (done) {
                // TODO: Remove afterMutations when we upgrade to skatejs 0.14.x/1.0.x
                helpers.afterMutations(function () {
                    clickTrigger(revealText);
                    expect(revealText.open).to.equal(true, 'opens on click');
                    done();
                });
            });

            it('shows the default closed text on initialisation', function () {
                expect(getTriggerText(revealText)).to.equal('Show more', 'shows default text');
            });

            it('shows the default open text after it is clicked', function (done) {
                helpers.afterMutations(function () {
                    clickTrigger(revealText);
                    expect(getTriggerText(revealText)).to.equal('Show less', 'shows default text after click');
                    done();
                });
            });

            it('can be opened and closed programmatically', function (done) {
                helpers.afterMutations(function () {
                    revealText.open = true;
                    expect(revealText.open).to.equal(true, 'opens programmatically');
                    revealText.open = false;
                    helpers.afterMutations(function () {
                        expect(revealText.open).to.equal(false, 'closes programmatically');
                        done();
                    });
                });
            });

            it('can be opened and closed by setting the "open" attribute', function (done) {
                revealText.setAttribute('open', '');
                helpers.afterMutations(function () {
                    expect(revealText.open).to.equal(true, 'opens on setting attribute');
                    revealText.removeAttribute('open');
                    helpers.afterMutations(function () {
                        expect(revealText.open).to.equal(false, 'closes on removing attribute');
                        done();
                    });
                });
            });

            it('updates the "open" attribute when toggled programmatically', function (done) {
                revealText.open = true;
                helpers.afterMutations(function () {
                    expect(revealText.hasAttribute('open')).to.equal(true, 'sets open attribute when opening');
                    done();
                });
            });

            it('triggers the aui-show event after opening', function (done) {
                const showEventSpy = sinon.spy();
                $(revealText).on('aui-show', showEventSpy);
                revealText.open = true;
                helpers.afterMutations(function () {
                    showEventSpy.should.have.been.calledOnce;
                    done();
                });
            });
        });

        describe('reveal-text opened by default', function () {
            beforeEach(function () {
                initialiseRevealText({open: true});
            });

            it('is initialised to be open', function () {
                expect(revealText.open).to.equal(true, 'starts open');
            });

            it('closes when clicked', function (done) {
                helpers.afterMutations(function() {
                    clickTrigger(revealText);
                    expect(revealText.open).to.equal(false, 'closes on click');
                    done();
                });
            });

            it('updates the "open" attribute when toggled programmatically', function(done) {
                revealText.open = false;
                helpers.afterMutations(function() {
                    expect(revealText.hasAttribute('open')).to.equal(false, 'removes open attribute when closing');
                    done();
                });
            });

            it('triggers the aui-hide event after opening', function(done) {
                const hideEventSpy = sinon.spy();
                $(revealText).on('aui-hide', hideEventSpy);
                revealText.open = false;
                helpers.afterMutations(function() {
                    hideEventSpy.should.have.been.calledOnce;
                    done();
                });
            });
        });

        describe('reveal-text with custom trigger text', function () {
            const customOpenText = 'custom open text';
            const customClosedText = 'custom closed text';

            describe('which starts closed', function () {
                beforeEach(function () {
                    initialiseRevealText({
                        openText: customOpenText,
                        closedText: customClosedText
                    });
                });

                it('is initialised as closed with custom closed text shown', function () {
                    expect(revealText.open).to.equal(false, 'starts closed');
                    expect(getTriggerText(revealText)).to.equal(customClosedText, 'displays custom text');
                });

                it('opens when trigger is clicked and shows the custom open text', function (done) {
                    helpers.afterMutations(function() {
                        clickTrigger(revealText);
                        expect(revealText.open).to.equal(true, 'opens on click');
                        expect(getTriggerText(revealText)).to.equal(customOpenText, 'displays custom text');
                        done();
                    });
                });
            });

            describe('which starts open', function () {
                beforeEach(function () {
                    initialiseRevealText({
                        open: true,
                        openText: customOpenText,
                        closedText: customClosedText
                    });
                });

                it('is initialised as open with custom open text shown', function () {
                    expect(revealText.open).to.equal(true, 'starts open');
                    expect(getTriggerText(revealText)).to.equal(customOpenText, 'displays custom text');
                });

                it('closes when trigger is clicked and shows the custom closed text', function (done) {
                    helpers.afterMutations(function () {
                        clickTrigger(revealText);
                        expect(revealText.open).to.equal(false, 'closes on click');
                        expect(getTriggerText(revealText)).to.equal(customClosedText, 'displays custom text');
                        done();
                    });
                });
            });

            describe('updates the open and closed text', function () {
                const newOpenText = 'New open text';
                const newClosedText = 'New closed text';

                beforeEach(function () {
                    initialiseRevealText();
                });

                function runTriggerTextTests() {
                    it('and shows the updated closed text', function (done) {
                        helpers.afterMutations(function () {
                            expect(getTriggerText(revealText)).to.equal(newClosedText, 'shows updated closed text');
                            done();
                        });
                    });

                    it('and shows the updated open text', function (done) {
                        revealText.open = true;
                        helpers.afterMutations(function () {
                            expect(getTriggerText(revealText)).to.equal(newOpenText, 'shows updated open text');
                            done();
                        });
                    });
                }

                describe('when the attribute is changed', function () {
                    beforeEach(function () {
                        revealText.setAttribute('open-text', newOpenText);
                        revealText.setAttribute('closed-text', newClosedText);
                    });

                    runTriggerTextTests();
                });

                describe('when the property is set', function () {
                    beforeEach(function () {
                        revealText.openText = newOpenText;
                        revealText.closedText = newClosedText;
                    });

                    runTriggerTextTests();
                });
            });
        });

        describe('trigger behaviour -', function () {
            beforeEach(function () {
                initialiseRevealText();
            });

            it('is initialised as closed with text and trigger visible', function () {
                const height = $(revealText).height();
                expect(revealText.open).to.equal(false, 'starts closed');
                expect(height).to.be.above(0, 'height greater than 0 when closed');
                expect($(getTrigger(revealText)).css('position')).to.equal('absolute', 'trigger has absolute position when closed');
            });

            it('opens when trigger is clicked to show all contents and moves the trigger to after the text', function (done) {
                helpers.afterMutations(function() {
                    const closedHeight = $(revealText).height();
                    clickTrigger(revealText);
                    const height = $(revealText).height();
                    expect(revealText.open).to.equal(true, 'opens on click');
                    expect(height).to.be.at.least(closedHeight, 'height when open greater than when closed');
                    expect($(getTrigger(revealText)).css('position')).to.equal('relative', 'trigger has relative position when open');
                    done();
                });
            });
        });
    });
});
