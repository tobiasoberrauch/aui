'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var karma = require('../../lib/karma');
var rootPaths = require('../../lib/root-paths');

var taskBuildTest = gat.load('test/build');

module.exports = gulp.series(
    taskBuildTest,
    function watch (done) {
        var srcPaths = rootPaths('src/**/*.js');
        var testPaths = rootPaths('tests/**/*.js');
        gulp.watch(srcPaths.concat(testPaths), taskBuildTest).on('change', galv.cache.expire);
        done();
    },
    function run (done) {
        karma(gat.opts({
            watch: true
        }), done).start();
    }
);
